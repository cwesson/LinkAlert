/**
 * @file storage.js
 * @author Conlan Wesson
 * Link Alert storage API wrapper.
 * @copyright Copyright (C) 2018
 */

class LinkAlertStorage {
	/**
	 * Stores the given options.
	 * @param opts Options to store.
	 */
	static store(opts){
		chrome.storage.local.set(opts);
	}
	
	/**
	 * Loads the options.
	 * @param callback Function to call with the loaded options.
	 */
	static load(callback){
		chrome.storage.local.get(null, (res) => {
			if(Object.keys(res).length === 0){
				storeOptions(defaultOptions);
				res = defaultOptions;
			}
			callback(res);
		});
	}
	
	/**
	 * Add a callback for when the options are changed.
	 * @param callback Function to call when the options change.
	 */
	static addListener(callback){
		chrome.storage.onChanged.addListener(callback);
	}
}

