/**
 * @file locale.js
 * @author Conlan Wesson
 * Functions for localizing pages.
 * @copyright Copyright (C) 2018
 */

/**
 * Scans the page for "locale" name and adds localized strings.
 */
function localize(){
	var list = document.getElementsByClassName("locale");
	for (var i = 0; i < list.length; i++) {
		var id = list[i].attributes["name"].value.split("-")[1];
		try{
			var text = document.createTextNode(chrome.i18n.getMessage(id));
			list[i].appendChild(text);
		}catch(e){}
	}
}

document.addEventListener('DOMContentLoaded', localize);

