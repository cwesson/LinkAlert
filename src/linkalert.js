/**
 * @file linkalert.js
 * @author Conlan Wesson
 * Main object for controlling Link Alert on a page.
 * @copyright Copyright (C) 2018
 */

/**
 * Link Alert
 * Changes the cursor to indicate the target of a link.
 */
var LinkAlert = {
	EXTENSIONS: {
		"doc": ".doc,.docx,:%LASKIN%/%LASIZE%/doc-icon.png;",
		"app": ".exe,.msi,:%LASKIN%/%LASIZE%/app-icon.png;",
		"img": ".jpg,.gif,.png,.bmp,:%LASKIN%/%LASIZE%/img-icon.png;",
		"jse": ".js,.jse,:%LASKIN%/%LASIZE%/jse-icon.png;",
		"aud": ".mp3,.wma,.wav,:%LASKIN%/%LASIZE%/aud-icon.png;",
		"vid": ".mpg,.wmv,.mkv,.avi,:%LASKIN%/%LASIZE%/vid-icon.png;",
		"pdf": ".pdf,:%LASKIN%/%LASIZE%/pdf-icon.png;",
		"ppt": ".ppt,.pps,.ppa,.pptx,:%LASKIN%/%LASIZE%/ppt-icon.png;",
		"reg": ".reg,:%LASKIN%/%LASIZE%/reg-icon.png;",
		"txt": ".txt,:%LASKIN%/%LASIZE%/txt-icon.png;",
		"torrent": ".torrent,:%LASKIN%/%LASIZE%/torrent-icon.png;",
		"xls": ".xls,.xlsx,.csv,:%LASKIN%/%LASIZE%/xls-icon.png;",
		"xpi": ".xpi,:%LASKIN%/%LASIZE%/xpi-icon.png;",
		"zip": ".zip,.rar,.gz,.7z,.tar,.tgz,.lzma,:%LASKIN%/%LASIZE%/zip-icon.png;",
		"pkg": ".pkg,.dmg,.rpm,.deb,:%LASKIN%/%LASIZE%/pkg-icon.png;",
		"cd":  ".iso,.img,:%LASKIN%/%LASIZE%/cd-icon.png;"
	},
	
	PROTOCOLS: {
		"chat": "aim,irc,icq,:%LASKIN%/%LASIZE%/chat-icon.png;",
		"ftp": "ftp,ftps,:%LASKIN%/%LASIZE%/ftp-icon.png;",
		"secure": "https,:%LASKIN%/%LASIZE%/secure-icon.png;",
		"unsecure": "http,:%LASKIN%/%LASIZE%/unsecure-icon.png;",
		"jsp": "javascript,:%LASKIN%/%LASIZE%/jsp-icon.png;",
		"mail": "mailto,:%LASKIN%/%LASIZE%/mail-icon.png;",
		"file": "file,:%LASKIN%/%LASIZE%/file-icon.png;"
	},
	
	DOMAINS: {
		"google.com": "%LASKIN%/%LASIZE%/google-icon.png",
		"youtube.com": "%LASKIN%/%LASIZE%/youtube-icon.png",
		"youtu.be": "%LASKIN%/%LASIZE%/youtube-icon.png",
		"facebook.com": "%LASKIN%/%LASIZE%/facebook-icon.png",
		"twitter.com": "%LASKIN%/%LASIZE%/twitter-icon.png",
		"twimg.com": "%LASKIN%/%LASIZE%/twitter-icon.png",
		"t.co": "%LASKIN%/%LASIZE%/twitter-icon.png",
		"reddit.com": "%LASKIN%/%LASIZE%/reddit-icon.png",
		"redd.it": "%LASKIN%/%LASIZE%/reddit-icon.png",
		"imgur.com": "%LASKIN%/%LASIZE%/imgur-icon.png",
		"wikipedia.org": "%LASKIN%/%LASIZE%/wikipedia-icon.png"
	},
	
	MOVE_TARGET: null,
	
	options: defaultOptions,
	
	lastTarget: null,
	lastEventTarget: null,
	box: null,
	observer: null,
	
	init: function(){
		// Add onMouseMove
		var links = document.getElementsByTagName("A");
		for (var i = 0; i < links.length; i++) {
			links[i].addEventListener("mouseover", LinkAlert.onOver);
			links[i].addEventListener("mousemove", LinkAlert.onMove);
			links[i].addEventListener("mouseout", LinkAlert.onOut);
		}
		
		// Listen for new nodes
		LinkAlert.observer = new MutationObserver(function(mutations) {
			mutations.forEach(function(mutation) {
				mutation.addedNodes.forEach(function(node) {
					node.addEventListener("mouseover", LinkAlert.onOver);
					node.addEventListener("mousemove", LinkAlert.onMove);
					node.addEventListener("mouseout", LinkAlert.onOut);
				});
			});
		});
		LinkAlert.observer.observe(document, { childList: true, subtree: true });
		
		// Load options
		LinkAlertStorage.addListener(LinkAlert.optionsChanged);
		LinkAlert.optionsChanged();
	},
	
	optionsChanged: function(changes, area){
		LinkAlertStorage.load((res) => {
			LinkAlert.options = res;
		});
	},
	
	onOver: function(e){
		var moveTarget = e.target;
		if(moveTarget != LinkAlert.lastEventTarget){
			LinkAlert.lastEventTarget = moveTarget;
			//Find <A> if not current node
			while(moveTarget.nodeName.toUpperCase() != "A"){
				moveTarget = moveTarget.parentNode;
				if(moveTarget == null || moveTarget.nodeName.toLowerCase() == "#document"){
					moveTarget = null;
					break;
				}
			}
		}else{
			moveTarget = LinkAlert.lastTarget;
		}
		LinkAlert.MOVE_TARGET = moveTarget;
		if(moveTarget == null || moveTarget.nodeName.toUpperCase() != "A"){
			LinkAlert.onOut(e);
			LinkAlert.lastTarget = moveTarget;
			return;
		}
		
		if(moveTarget != LinkAlert.lastTarget){
			LinkAlert.box = new LinkAlertMouseBox(e);
		}
		
		var iconSrc = LinkAlert.getIcons(moveTarget);
		
		if(iconSrc[0] !== ""){
			LinkAlert.box.show(iconSrc);
		}else{
			LinkAlert.onOut();
		}
		LinkAlert.lastTarget = moveTarget;
	},
	
	onMove: function(e){
		if(LinkAlert.box != null && LinkAlert.MOVE_TARGET != null){
			LinkAlert.box.move(e);
		}
	},
	
	onOut: function(e){
		if(LinkAlert.box != null){
			LinkAlert.box.hide();
		}
		LinkAlert.MOVE_TARGET = null;
	},
	
	getIcons: function(moveTarget){
		var useNumber = LinkAlert.options["display"]["number"];
		var priority = LinkAlert.options["display"]["priority"];
		var showExternal = LinkAlert.options["special"]["external"];
		var showInternal = LinkAlert.options["special"]["internal"];
		var showDomains = LinkAlert.options["special"]["domains"];
		var showNewWindow = LinkAlert.options["special"]["new"];
		var showRss = LinkAlert.options["special"]["rss"];
		var showOffline = LinkAlert.options["special"]["offline"];
		var showCached = LinkAlert.options["special"]["cached"];
		var prefExtensions = LinkAlert.options["extensions"];
		var prefProtocols = LinkAlert.options["protocols"];
		var protoIfDiff = LinkAlert.options["protocols"]["ifDifferent"];
		var customExtensions = LinkAlert.options["advanced"]["ext-paths"];
		var customProtocols = LinkAlert.options["advanced"]["proto-paths"];
		
		//Merge default and custom icons
		var showExtensions = "";
		for(var ext in LinkAlert.EXTENSIONS){
			if(LinkAlert.EXTENSIONS.hasOwnProperty(ext)){
				if(prefExtensions[ext]){
					showExtensions += LinkAlert.EXTENSIONS[ext];
				}
			}
		}
		for(var path in customExtensions){
			if(customExtensions.hasOwnProperty(path)){
				showExtensions += customExtensions[path].join(",") + ",:"+path+";";
			}
		}
		
		var showProtocols = "";
		for(var proto in LinkAlert.PROTOCOLS){
			if(LinkAlert.PROTOCOLS.hasOwnProperty(proto)){
				if(prefProtocols[proto]){
					showProtocols += LinkAlert.PROTOCOLS[proto];
				}
			}
		}
		for(var path in customProtocols){
			if(customProtocols.hasOwnProperty(path)){
				showProtocols += customProtocols[path].join(",") + ",:"+path+";";
			}
		}
		
		//Get link info
		var linkHref = LinkAlert.getAddress(moveTarget.href);
		var linkProtocol = moveTarget.protocol.toLowerCase();
		linkProtocol = linkProtocol.substr(0,linkProtocol.length-1);
		var linkDomain = LinkAlert.getDomain(moveTarget.hostname).toLowerCase();
		var linkExtension = LinkAlert.getExtension(linkHref).toLowerCase();
		var linkTarget = moveTarget.target.toLowerCase();
		var pageHref = LinkAlert.getAddress(document.location.href);
		var pageProtocol = document.location.protocol.toLowerCase();
		pageProtocol = pageProtocol.substr(0,pageProtocol.length-1);
		var pageDomain = LinkAlert.getDomain(document.location.hostname).toLowerCase();
		var isInternal = ((pageHref == linkHref) && (moveTarget.href.indexOf("#") != -1));
		
		var iconSrc = new Array();
		for(var i = 0; i < useNumber; i++){
			iconSrc[i] = "";
		}
		
		//Process priority order
		var iconAt = 0;
		for(var i = 0; i < priority.length && iconAt < useNumber; i++){
			var setIcon = false;
			switch(priority[i]){
				case "inex":
					//Check for external link
					if(showExternal && linkDomain != pageDomain && linkDomain !== ""){
						if(showDomains && LinkAlert.DOMAINS[linkDomain]){
							iconSrc[iconAt] = LinkAlert.DOMAINS[linkDomain];
						}else{
							iconSrc[iconAt] = "%LASKIN%/%LASIZE%/external-icon.png";
						}
						setIcon = true;
					//Check for internal link 
					}else if(showInternal && isInternal){
						iconSrc[iconAt] = "%LASKIN%/%LASIZE%/internal-icon.png";
						setIcon = true;
					}
					break;
				case "newWindow":
					//Check New Window
					if(showNewWindow && (linkTarget == "_blank" || linkTarget == "_new" || linkTarget == "_newwindow")){
						iconSrc[iconAt] = "%LASKIN%/%LASIZE%/new-icon.png";
						setIcon = true;
					}
					break;
				case "proto":
					//Check Protocol
					var x = showProtocols.lastIndexOf(linkProtocol + ",");
					if(x != -1 && linkProtocol !== "" && linkHref !== "" && (!protoIfDiff || linkProtocol != pageProtocol)){
						var u = showProtocols.indexOf(":",x);
						var n = showProtocols.indexOf(";",u);
						iconSrc[iconAt] = showProtocols.substr(u+1,n-u-1);
						setIcon = true;
					}
					break;
				case "ext":
					//Check File Extension
					linkExtension = ("." + linkExtension + ",");
					var x = showExtensions.lastIndexOf(linkExtension);
					if(x != -1 && linkExtension !== ""){
						var u = showExtensions.indexOf(":",x);
						var n = showExtensions.indexOf(";",u);
						iconSrc[iconAt] = showExtensions.substr(u+1,n-u-1);
						setIcon = true;
					}else if(showRss && linkHref.match(/(?:\b|_)rss(?=\b|_)/)){
						iconSrc[iconAt] = "%LASKIN%/%LASIZE%/rss-icon.png";
						setIcon = true;
					}
					break;
				case "offline":
					//Check for cached link
					if(linkProtocol != "file"){
						if(showCached && !isInternal){
							var isCached = false;
							let {LoadContextInfo} = Components.utils.import("resource://gre/modules/LoadContextInfo.jsm", {});
							let {PrivateBrowsingUtils} = Components.utils.import("resource://gre/modules/PrivateBrowsingUtils.jsm", {});
							var cacheService = Components.classes["@mozilla.org/netwerk/cache-storage-service;1"].getService(Components.interfaces.nsICacheStorageService);
							var storage = cacheService.diskCacheStorage(
							  LoadContextInfo.fromLoadContext(PrivateBrowsingUtils.privacyContextFromWindow(window, false), false), false
							);
							try{isCached = storage.exists(makeURI(moveTarget.href), "");}catch(e){}
							if(isCached){
								iconSrc[iconAt] = "%LASKIN%/%LASIZE%/cached-icon.png";
								setIcon = true;
							}
						}
						if(showOffline && !setIcon && !navigator.onLine){
							iconSrc[iconAt] = "%LASKIN%/%LASIZE%/offline-icon.png";
							setIcon = true;
						}
					}
					break;
				default:
					break;
			}
			if(setIcon){
				iconSrc[iconAt] = LinkAlert.replacePath(iconSrc[iconAt], moveTarget);
				iconAt++;
			}
		}
		
		return iconSrc;
	},
	
	replacePath: function(path, moveTarget){
		var useLarge = LinkAlert.options["display"]["large"];
		var linkProtocol = moveTarget.protocol.toLowerCase();
		var linkExtension = LinkAlert.getExtension(moveTarget.href);
		linkProtocol = linkProtocol.substr(0,linkProtocol.length-1);
		
		path = path.replace(/%LASKIN%/g,"icons");
		if(useLarge){
			path = path.replace(/%LASIZE%/g,"large");
			path = path.replace(/%LASIZEPX%/g,"32");
		}else{
			path = path.replace(/%LASIZE%/g,"small");
			path = path.replace(/%LASIZEPX%/g,"16");
		}
		path = path.replace(/%LAEXT%/g,linkExtension);
		path = path.replace(/%LAPROTO%/g,linkProtocol);
		
		return path;
	},
	
	getAddress: function(uri){
		//Remove parameters
		while(uri.indexOf("?") > -1){
			uri = uri.substr(0,uri.indexOf("?"));
		}
		while(uri.indexOf("#") > -1){
			uri = uri.substr(0,uri.indexOf("#"));
		}
		return uri;
	},
	
	getDomain: function(host){
		while(host.indexOf(".") != host.lastIndexOf(".")){
			host = host.substr(host.indexOf(".")+1);
		}
		return host;
	},
	
	getExtension: function(uri){
		uri = LinkAlert.getAddress(uri).toLowerCase();
		var extension = uri.substr(uri.lastIndexOf(".")+1);
		return extension;
	}
	
}// END LinkAlert

//Set onLoad Listner
LinkAlert.init();

