/**
 * @file defaults.js
 * @author Conlan Wesson
 * Default options for Link Alert.
 * @copyright Copyright (C) 2018
 */

var defaultOptions = {
	// File extensions
	"extensions": {
		"doc": true,
		"app": true,
		"img": true,
		"jse": true,
		"aud": true,
		"vid": true,
		"pdf": true,
		"ppt": true,
		"reg": true,
		"txt": true,
		"torrent": true,
		"xls": true,
		"xpi": true,
		"zip": true,
		"pkg": true,
		"cd":  true
	},
	// Protocols
	"protocols": {
		"chat": true,
		"ftp": true,
		"secure": true,
		"unsecure": true,
		"jsp": true,
		"mail": true,
		"file": true,
		"ifDifferent": true
	},
	// Misc
	"special": {
		"external": true,
		"internal": true,
		"new": true,
		"rss": true,
		"offline": false,
		"cached": false,
		"domains": true
	},
	// Custom icons
	"advanced": {
		"ext-paths": {
		},
		"proto-paths": {
		}
	},
	"offset": {
		"x": 10,
		"y": 10
	},
	"display": {
		"usebg": true,
		"useborder": true,
		"useshadow": true,
		"bgcolor": "#FFFFCC",
		"border": "#000000",
		"borderwidth": 1,
		"opacity": 100,
		"radius": 0,
		"number": 5,
		"large": false,
		"priority": ["offline", "ext", "proto", "inex", "newWindow"]
	}
};

