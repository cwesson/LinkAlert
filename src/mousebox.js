/**
 * @file mousebox.js
 * @author Conlan Wesson
 * Object for displaying icons in an HTML div on the page.
 * @copyright Copyright (C) 2018
 */

LinkAlertMouseBox.prototype.event = null;
LinkAlertMouseBox.prototype.iconBox = null;
LinkAlertMouseBox.prototype.padding = 2;

function LinkAlertMouseBox(e){
	this.event = e;
}

LinkAlertMouseBox.prototype.show = function(iconSrc){
	var useLarge = LinkAlert.options["display"]["large"];
	
	//Check if box exists yet
	this.iconBox = document.getElementById("linkalert-box");
	if (!this.iconBox){
		this.addIconBox();
	}
	
	//Change Icons
	var boxWidth = 0;
	for(var i = 0; i < iconSrc.length; i++){
		var iconImage = document.getElementById("linkalert-icon-"+(i+1));
		var iconStyle = iconImage.style;
		iconStyle.setProperty("left", (boxWidth + this.padding) + "px", "important");
		if(iconSrc[i] !== ""){
			if(useLarge){
				iconStyle.setProperty("width", "32px", "important");
				iconStyle.setProperty("height", "32px", "important");
				boxWidth += 32;
			}else{
				iconStyle.setProperty("width", "16px", "important");
				iconStyle.setProperty("height", "16px", "important");
				boxWidth += 16;
			}
			iconImage.setAttribute("src", chrome.extension.getURL(iconSrc[i]));
		}else{
			iconImage.setAttribute("src","");
			iconStyle.setProperty("width", "0px", "important");
			iconStyle.setProperty("height", "0px", "important");
		}
	}
	
	//Show icons
	if(useLarge){
		this.iconBox.style.setProperty("height", (32+(this.padding*2))+"px", "important");
	}else{
		this.iconBox.style.setProperty("height", (16+(this.padding*2))+"px", "important");
	}
	this.iconBox.style.setProperty("width", (boxWidth+(this.padding*2)) + "px", "important");
	this.move(this.event);
}

LinkAlertMouseBox.prototype.hide = function(){
	if(this.iconBox){
		document.body.removeChild(this.iconBox);
		this.iconBox = null;
	}
}

LinkAlertMouseBox.prototype.move = function(e){
	var offsetX = parseInt(LinkAlert.options["offset"]["x"]);
	var offsetY = parseInt(LinkAlert.options["offset"]["y"]);
	this.event = e;
	
	//Code provided by Dani Church
	var frameX = 0;
	var frameY = 0;
	var isFixed = false;
	var currentNode = LinkAlert.MOVE_TARGET;
	// Check to see if anything in the link's ancestry is fixed
	while (currentNode) {
		if (window.getComputedStyle(currentNode, "").position == "fixed") {
			isFixed = true;
			break;
		}
		currentNode = currentNode.offsetParent;
	}
	currentNode = LinkAlert.MOVE_TARGET.ownerDocument.defaultView.frameElement; //Check for inside a frame
	while(currentNode){
		if(!isFixed){
			frameX -= currentNode.contentWindow.scrollX;
			frameY -= currentNode.contentWindow.scrollY;
		}
		isFixed = false;
		while(currentNode && currentNode.nodeName.toLowerCase() !== "#document"){
			var curStyle = window.getComputedStyle(currentNode, "");
			frameX += currentNode.offsetLeft;
			frameY += currentNode.offsetTop;
			if(currentNode.nodeName.toLowerCase() != "body"){
				frameX -= currentNode.scrollLeft;
				frameY -= currentNode.scrollTop;
			}
			if(!currentNode.offsetParent){
				currentNode = currentNode.parentNode;
			}else if (curStyle.position == "fixed"){
				isFixed = true;
				currentNode = currentNode.ownerDocument;
			}else if(curStyle.position == "absolute"){
				currentNode = currentNode.offsetParent;
			}else{
				var scrollNode = currentNode.parentNode;
				currentNode = currentNode.offsetParent;
				while(scrollNode && scrollNode != currentNode){
					frameX -= scrollNode.scrollLeft;
					frameY -= scrollNode.scrollTop;
					scrollNode = scrollNode.parentNode;
				}
			}
		}
		if(currentNode){
			currentNode = currentNode.defaultView.frameElement; //Find next higher frame
		}
	}
	if(isFixed){
		frameX += document.body.scrollLeft;
		frameY += document.body.scrollTop;
	}
	//END Dani Church's code
	
	//Get X,Y Coords of Mouse
	var iconX = this.event.clientX + offsetX + frameX;
	var iconY = this.event.clientY + offsetY + frameY;
	
	// Account very rare pages with body offset
	if(window.getComputedStyle(document.body, null).getPropertyValue("position") === "absolute"){
		var bodyBox = document.body.getBoundingClientRect();
		if(bodyBox.left){
			iconX -= bodyBox.left;
		}
		if(bodyBox.top){
			iconY -= bodyBox.top;
		}
		iconX -= window.scrollX;
		iconY -= window.scrollY;
	}
	
	//Correct Off the edge
	var boxWidth = this.iconBox.offsetWidth;
	var boxHeight = this.iconBox.offsetHeight;
	var documentWidth = window.innerWidth;
	var documentHeight = window.innerHeight;
	if(iconX + boxWidth > documentWidth){
		iconX -= (2 * offsetX) + boxWidth;
	}
	if(iconY + boxHeight > documentHeight){
		iconY -= (2 * offsetY) + boxHeight;
	}
	
	//Move Icon
	this.iconBox.style.setProperty("left", window.pageXOffset + iconX + "px", "important");
	this.iconBox.style.setProperty("top", window.pageYOffset + iconY + "px", "important");
}

LinkAlertMouseBox.prototype.addIconBox = function(){
	var useBackground = LinkAlert.options["display"]["usebg"];
	var useBackgroundColor = LinkAlert.options["display"]["bgcolor"];
	var useBorder = LinkAlert.options["display"]["useborder"];
	var useBorderColor = LinkAlert.options["display"]["border"];
	var useBorderWidth = LinkAlert.options["display"]["borderwidth"];
	var useShadow = LinkAlert.options["display"]["useshadow"];
	var useOpacity = LinkAlert.options["display"]["opacity"];
	var useRadius = LinkAlert.options["display"]["radius"];
	
	// Build up the node
	var iconBox = document.createElement("DIV");
	iconBox.setAttribute("id","linkalert-box");
	iconBox.setAttribute("class","linkalert-box");

	// Override all the things
	var iconStyle = iconBox.style;
	iconStyle.setProperty("border-radius", useRadius+"px", "important");

	if(useBackground){
		iconStyle.setProperty("background-color", useBackgroundColor, "important");
		if(useShadow){
			iconStyle.setProperty("box-shadow", "2px 2px 3px rgba(0,0,0,0.5)", "important");
		}
	}
	if(useBorder){
		iconStyle.setProperty("border", useBorderColor + " " + useBorderWidth + "px solid", "important");
	}
	iconStyle.setProperty("opacity", useOpacity/100, "important");

	// Add icons to the box
	for(var i = 0; i < 5; i++){
		var iconImage = document.createElement("IMG");
		iconImage.setAttribute("id","linkalert-icon-"+(i+1));
		iconImage.setAttribute("class","linkalert-icon");
		iconImage.setAttribute("src", chrome.extension.getURL("icons/none-icon.png"));
		iconBox.appendChild(iconImage);
	}

	// Add box to the page
	document.body.appendChild(iconBox);
	this.iconBox = iconBox;
}

