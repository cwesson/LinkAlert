# LinkAlert Change Log

## v2.0.0
* Switch to WebExtensions.  This looses all preferences.
* Improve positioning and size of icons.
* Add shadow and rounded corners options.
* Add more file extensions and protocols by default.
