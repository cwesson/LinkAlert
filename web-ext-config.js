module.exports = {
	verbose: true,
	build: {
		overwriteDest: true,
	},
	ignoreFiles: [
		"web-ext-config.js",
		"Makefile",
		"*.in",
		"*.md",
		"*.log",
		"scripts/"
	],
	artifactsDir: "./"
};
