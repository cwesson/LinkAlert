.PHONY: all clean realclean

VERSION ?= $(patsubst v%,%,$(subst -,.,$(shell git describe --tags | sed 's/v//' | sed 's/\-g.*//')))
XPI := link_alert-$(VERSION).xpi
SRCS := $(shell find icons/) $(shell find _locales/) $(shell find src/)
BINS := manifest.json

ifeq ($(VERBOSE),)
	BUILD_LOG := > build.log
	LINT_LOG := > lint.log
endif

all: $(BINS) $(XPI)

%.json: %.json.in
	@echo "PRE " $@
	@cpp -P -C -nostdinc -DVERSION=\"$(VERSION)\" -o $@ $<

$(XPI): $(SRCS) $(BINS)
	@echo "XPI " $@
	@web-ext build $(BUILD_LOG)
	@mv $(patsubst %.xpi,%.zip,$(XPI)) $(XPI)

clean:
	@echo "RM  " $(XPI)
	@rm -f $(XPI)
	@echo "RM  " $(BINS)
	@rm -f $(BINS)

realclean: clean
	@echo "RM   *.xpi"
	@rm -f *.xpi

lint:
	@echo "LINT"
	@web-ext lint --output=json --pretty $(LINT_LOG)

