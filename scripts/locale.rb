#!/usr/bin/env ruby

require 'json'
require 'fileutils'

lookup = Hash[
	"basic" => "basic",
	"ext" => "ext",
	"basic.app" => "app",
	"basic.torrent" => "torrent",
	"basic.cd" => "cd",
	"basic.xls" => "xls",
	"basic.img" => "img",
	"basic.jse" => "jse",
	"basic.xpi" => "xpi",
	"basic.aud" => "aud",
	"basic.pkg" => "pkg",
	"basic.pdf" => "pdf",
	"basic.ppt" => "ppt",
	"basic.reg" => "reg",
	"basic.txt" => "txt",
	"basic.vid" => "vid",
	"basic.doc" => "doc",
	"basic.zip" => "zip",
	"proto" => "proto",
	"basic.chat" => "chat",
	"basic.mail" => "mail",
	"basic.ftp" => "ftp",
	"basic.jsp" => "jsp",
	"basic.secure" => "secure",
	"basic.unsecure" => "unsecure",
	"basic.file" => "file",
	"basic.ifDifferent" => "ifDifferent",
	"basic.external" => "external",
	"basic.internal" => "internal",
	"basic.domains" => "domains",
	"newWindow" => "newWindow",
	"basic.rss" => "rss",
	"offline" => "offline",
	"offline.cached" => "cached",
	"disp" => "display",
	"disp.large" => "large",
	"disp.priority" => "priority",
	"disp.number" => "number",
	"disp.offset" => "offset",
	"priorityoffline" => "offcache",
	"priorityext" => "inex",
	"disp.offset.x" => "x",
	"disp.offset.y" => "y",
	"disp.boxstyle" => "boxstyle",
	"disp.bgcolor" => "background",
	"disp.border.color" => "border",
	"disp.shadow" => "shadow",
	"disp.border.width" => "width",
	"disp.opacity" => "opacity",
	"disp.radius" => "radius",
	"disp.px" => "px",
	"disp.percent" => "percent",
	"adv" => "advanced",
	"adv.path" => "path"
]

output = Hash.new()

File.open("chrome/locale/"+ARGV[0]+"/preferences.dtd") do |file|
	file.each do |line|
		name, str = /linkalert\.([\w\.]+)\s\"(.+)\"/.match(line).captures
		if lookup[name]
			output[lookup[name]] = str
		end
	end
end
File.open("chrome/locale/"+ARGV[0]+"/preferences.properties") do |file|
	file.each do |line|
		name, str = /([\w\.]+)\=(.+)/.match(line).captures
		if lookup[name]
			output[lookup[name]] = str
		end
	end
end

en = JSON.parse(File.read("_locales/en/messages.json"))

FileUtils::mkdir_p("_locales/"+ARGV[0])

file = File.open("_locales/"+ARGV[0]+"/messages.json",'w')
file.write("{\n")
first = true
lookup.each do |k, v|
	key = v
	if output[v]
		value = output[v]
	else
		value = en[v]["message"]
		puts value
	end
	if not first
		file.write(",\n")
	end
	first = false
	file.write("	\""+key+"\":{\n")
	file.write("		\"message\": \""+value+"\",\n")
	file.write("		\"description\": \"\"\n")
	file.write("	}")
end
file.write("\n}\n")
file.close

