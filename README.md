# LinkAlert
LinkAlert is an add-on for Mozilla Firefox that displays an icon next to the cursor indicating the target of a link.

Indicates links to the following:
* New Windows
* Secure Sites
* Email Links
* JavaScript
* Word Documents
* Excel Spreadsheets
* PDF Files
* Zip Files
* Applications
* Text Files
* Images
* Chat Links

and more...

## Bug Reporting
To report bugs in LinkAlert, visit the issue tracker on [GitLab](https://gitlab.com/cwesson/LinkAlert/issues).  Please check for similar issues before submitting a new issue.

## Building
Building LinkAlert alert requires the the following be installed:
* make
* web-ext

To build LinkAlert simply run `make all`.  This will generate a .xpi file that can be installed in Firefox.

## Contributing
To contribute to LinkAlert, please submit a merge request on [GitLab](https://gitlab.com/cwesson/LinkAlert/merge_requests).
